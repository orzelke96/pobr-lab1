# Laboratorium 1 - POBR!

## Zadanie 1

Celem zadania była modyfikacja poniższych obrazów za pomocą dostępnych funkcji CorelPhoto Paint aby wyświetlały się tylko czerwone kaski robotników na zdjęciach:

 - 063019B.JPG 
 - 063056B.JPG

oraz aby na zdjęciu 138040.JPG był widoczny tylko niebieski fotel.

W tym celu w ustawieniach *barwa-nasycenie-jasność* została zmodyfikowana wartość dla koloru czerwonego (w przypadku zdjęcia kasków) oraz dla niebieskiego (zdjęcie fotela) dla wartości:

 1. 1
 2. 98
 3. 37

Jasność dla reszty kolorów została zmniejszona do minimum, oraz wartość dla intensywność w ustawieniach *Jaskrawość - Kontrast - Intensywność*. 



Dane wejściowe znajdują się w folderze initialPhotos, natomiast dane wyjściowe w folderze resultPhotos.


## Zadanie 2

Celem było stworzenie programu w C++ do modyfikacji zdjęcia Lena.png, w celu podzielenia obrazu na 4 części. Trzy z wydzielonych części zawierają modyfikacje:

 - Górna część ma podwyższony kontrast poprzez pomnożenie wartości barw RGB przez czynnik X (podany przez użytkownika).
 - Lewa część ma zmienioną barwę poprzez ustawienie bary czerwonej pomniejszonej o wartość *(R+G+B)/3*.
 - Dolna część ma zmienioną jasność poprzez dodanie do każdej wartości koloru RGB wartości współczynnika Y (podanego przez użytkownika).

Program na koniec wypisuje histogram jasności dla funkcji *(R+G+B)/3*.
