﻿#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>

int checkQuarter(int& row, int& col, int& rowMax, int& colMax) {
    if (row < col && row <= rowMax - col) {
        return 1;
    }
    else if (row < col && row > rowMax - col) {
        return 2;
    }
    else if (row >= col && row > rowMax - col) {
        return 3;
    }
    else if (row >= col && row <= rowMax - col) {
        return 4;
    }
}

int addToTheColor(int colorVal, double& addedVal) {
    if (colorVal + addedVal > 255) {
        colorVal = 255;
    }
    else if (colorVal + addedVal < 0) {
        colorVal = 0;
    }
    else
    {
        colorVal += addedVal;
    }
    return colorVal;
}

int multiplyTheColor(int colorVal, double& multiplyFactor) {
    if (colorVal * multiplyFactor > 255) {
        colorVal = 255;
    }
    else if (colorVal * multiplyFactor < 0) {
        colorVal = 0;
    }
    else
    {
        colorVal *= multiplyFactor;
    }
    return colorVal;
}

cv::Mat changeColor(cv::Mat& img) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    switch (img.channels()) {
    case 3:
        cv::Mat_<cv::Vec3b> _I = img;
        cv::Mat_<cv::Vec3b> _R = res;
        double x = 0;
        double y = 0;
        std::cout << "* Enter the X value [0.0-2.0]: "; std::cin >> x;
        std::cout << "* Enter the Y value [-100.00 - 100.00]: ";    std::cin >> y;
        int grey = 0;
        for (int i = 0; i < img.rows; ++i)
            for (int j = 0; j < img.cols; ++j) { 
                switch (checkQuarter(i, j, img.rows, img.cols)) {
                case 1:
                    _R(i, j)[0] = multiplyTheColor(_I(i, j)[0], x);
                    _R(i, j)[1] = multiplyTheColor(_I(i, j)[1], x);
                    _R(i, j)[2] = multiplyTheColor(_I(i, j)[2], x);
                    break;
                case 2:
                    _R(i, j)[0] = _I(i, j)[0];
                    _R(i, j)[1] = _I(i, j)[1];
                    _R(i, j)[2] = _I(i, j)[2];
                    break;
                case 3:

                    _R(i, j)[0] = addToTheColor(_I(i, j)[0], y);
                    _R(i, j)[1] = addToTheColor(_I(i, j)[1], y);
                    _R(i, j)[2] = addToTheColor(_I(i, j)[2], y);
                    break;
                case 4: 
                    grey = (_I(i, j)[0] + _I(i, j)[1] + _I(i, j)[2]) / 3;

                    _R(i, j)[0] = _I(i, j)[0] + grey;
                    _R(i, j)[1] = _I(i, j)[0] + grey;
                    _R(i, j)[2] = _I(i, j)[0] + grey;
                    break;
                }
                
            }
        res = _R;
        break;
    }
    return res;
}


double calculateHistogram(int r, int g, int b) {
    return (r + g + b) / 3;
}

void readHistogram(cv::Mat& img) {
    int pixelCount = 0;
    int histogramData[8];
    for (int i = 0; i < 8; i++) {
        histogramData[i] = 0; //null the histogram data table.
    }
    cv::Mat_<cv::Vec3b> _I = img;
    double histOfOnePixel = 0;
    for (int i = 0; i < img.rows; ++i)
        for (int j = 0; j < img.cols; ++j) {
            histOfOnePixel = calculateHistogram(_I(i, j)[0], _I(i, j)[1], _I(i, j)[2]);
            if (histOfOnePixel < 31)
                histogramData[0]++;
            else if (histOfOnePixel >= 31 && histOfOnePixel < 63) histogramData[1]++;
            else if (histOfOnePixel >= 63 && histOfOnePixel < 95) histogramData[2]++;
            else if (histOfOnePixel >= 95 && histOfOnePixel < 127) histogramData[3]++;
            else if (histOfOnePixel >= 127 && histOfOnePixel < 159) histogramData[4]++;
            else if (histOfOnePixel >= 159 && histOfOnePixel < 191) histogramData[5]++;
            else if (histOfOnePixel >= 191 && histOfOnePixel < 223) histogramData[6]++;
            else if (histOfOnePixel >= 223 && histOfOnePixel <= 255) histogramData[7]++;
            pixelCount++;
        }
    //Reading histogram, probably could be made in the loop...
    std::cout << "Pikseli z zakresu 0 - 31 jest " << histogramData[0] << std::endl;
    std::cout << "Pikseli z zakresu 31 - 63 jest " << histogramData[1] << std::endl;;
    std::cout << "Pikseli z zakresu 63 - 95 jest " << histogramData[2] << std::endl;;
    std::cout << "Pikseli z zakresu 95 - 127 jest " << histogramData[3] << std::endl;;
    std::cout << "Pikseli z zakresu 127 - 159 jest " << histogramData[4] << std::endl;;
    std::cout << "Pikseli z zakresu 159 - 191 jest " << histogramData[5] << std::endl;;
    std::cout << "Pikseli z zakresu 191 - 223 jest " << histogramData[6] << std::endl;;
    std::cout << "Pikseli z zakresu 223 - 255 jest " << histogramData[7] << std::endl;;
    std::cout << "Suma pixeli to  " << pixelCount;
}




int main(int, char* []) {
    std::cout << "Start ..." << std::endl;
    cv::Mat image = cv::imread("Lena.png");
    cv::Mat res = changeColor(image);
    readHistogram(res);
    cv::imshow("Lena", res);
    cv::waitKey(-1);
    return 0;
}
